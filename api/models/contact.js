'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Contact Schema for Mongoose
let ContactSchema = new Schema({
    //Atributes and their constraints
    //First-Name
    firstname: {
        type: String,
        required: "First-Name is required"
    },
    //Last-Name
    lastname: {
        type: String,
        required: "Last-Name is required"
    },
    //Phone Number
   phone: {
       type: String,
       required: "Phone is required"
   },
    //Email
    email: {
        type: String
    },
    //Date-Of-Creation
    created_date: {
        type: Date,
        default: Date.now
    }
}, {
    versionKey: false
});

module.exports = mongoose.model('contacts', ContactSchema);