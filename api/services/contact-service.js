//Setting up Service for API to interact with DB
'use strict';
const mongoose = require('mongoose'),
Contact = mongoose.model('contacts');

//Service to retrive all contacts from DB
exports.getAll = (() => {
    const promise = Contact.find().exec()
    return promise;
});

//Service to add contact to DB
exports.save = ((contact) => {
    const newContact = new Contact(contact);
    const promise = newContact.save();
    return promise;
});

//Service to fetch contact from DB using contact-ID
exports.get = ((contactId) => {
    const promise = Contact.findById(contactId).exec();
    return promise
});