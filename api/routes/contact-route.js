//Setting routes for the API
'use strict';
module.exports = ((app) => {
    const contactController = require('../controllers/contact-controller');
    //Routes to add and retrieve contact
    app.route('/contacts')
        .get(contactController.allContacts) //retrieve all contacts from DB
        .post(contactController.post);//add new contact to DB

    //Route to retrieve a particular contact from DB based on 'id'
    app.route('/contacts/:contactId')
        .get(contactController.get);
});