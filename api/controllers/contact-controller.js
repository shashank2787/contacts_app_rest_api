//Setting up Controller for the API
'use strict';
//Import service for the API
const contactService = require('../services/contact-service');
//Method to retrieve all contacts from DB
exports.allContacts = ((req, res) => {
    console.log('controller-allContacts');
    const resolve = (contacts) => {
        res.status(200);
        res.json(contacts);
    };
    contactService.getAll({})
        .then(resolve)
        .catch(renderErrorRes(res));
});

//Method to add contact to DB
exports.post = ((req, res) => {
    console.log('controller-POST');
    const newcontact = Object.assign({}, req.body);
    const resolve = (contact) => {
        res.status(200);
        res.json(contact);
    };
    contactService.save(newcontact)
        .then(resolve)
        .catch(renderErrorRes(res));
});

//Method to retreive contact based on contact-ID
exports.get = ((req, res) => {
    console.log('controller-get');
    const resolve = (contact) => {
        res.status(200);
        res.json(contact);
    };
    contactService.get(req.params.contactId)
        .then(resolve)
        .catch(renderErrorRes(res));
});

//Method to throw error to the calling function
let renderErrorRes = (res) => {
    const errorCallback = (error) => {
        if (error) {
            console.log('controller-renderErrorRes');
            res.status(500);
            res.json({
                message: error.message
            });
        }
    }
    return errorCallback;
};