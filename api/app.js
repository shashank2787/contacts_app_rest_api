//Initialize model and routes for the API
'use strict';
module.exports = ((app) => {
    //Initialize models
    const contactModel = require('./models/contact');

    //Initialize routes
    const contactRoutes = require('./routes/contact-route');
    contactRoutes(app);
});