//Initialize server and API properties
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors=require('cors');

//MongoDB Connection
mongoose.connect('mongodb+srv://dbuser:dbuser@cluster0-jdyid.mongodb.net/test?retryWrites=true', 
//mongoose.connect('mongodb://localhost:27017/contactDB', 
{
    //useMongoClient: true
    useNewUrlParser: true
});

//Logging and Parsing
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//Enable CORS
app.use(cors());

//Initialize API
let initApp = require('./api/app');
initApp(app);

app.listen(port);
console.log('AddressBook Server started on: ' + port);

//Error-Handling block for resource not found on server
app.use((req, res, next) =>{
    const error= new Error("Not found!!!");
    error.status = 404;
    next(error);
});

