#assignment-8-shanks2710
# Assignment 8
This project aims at creating a server that hosts a RESTful API that allows a Contact/PhoneBook app to:
1) View all contacts
2) Add a new Contact/PhoneBook
3) View a specific contact

# Prerequisites
This project requires npm to install plugins for its dependencies and to run commands to operate the server.
It uses features of different JavaScript libraries like mongoose, body-parser, morgan and JS-based framework ExpressJS to create RESTful services

# Built With
Visual Source Code - IDE to create project.
MongoDB : Database to store the application data and retrieve it at a later time
MongoDB-Compass Community Edition : To access database and check values
Postman : Tool to make HTTP request using different methods like GET, POST and view their response

# Run
Steps:
1) Clone the GitHub repo: https://github.com/neu-mis-info6150-spring-2019/assignment-8-shanks2710.git
2) Open Visual Source Code and Select File->Open Folder
3) Navigate to the respective folder and Click Select Folder
4) Go to Terminal Section
5) Run the command to initialize npm: npm init
6) Run the command to install the dependencies as mentioned in package.json file: npm install
7) Run the command to start the server: npm start
8) Server is started and a log 'AddressBook RESTful API server started on: 3000' is printed on the Terminal
9) To stop the server, run command 'Ctrl+C' and type 'Y' in the Terminal

# Author
Shashank Sharma, NU-ID: 001415411

# Acknowledgments
Prof. Amuthan Arulraj and all TAs


